
<?php
    require_once('vendor/autoload.php');
    require_once('classes/autoload.php');
    $pdfExport = new EXT_TCPDF('P','mm','A4',true,'UTF-8',false);
    $pdfExport->generate(new Data(new Supplier(),new Customer(),new Item()));
?>