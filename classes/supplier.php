<?php

    class Supplier {
        public $name,$addrStreet,$addrCity,$addrZip,$regCode,$vatCode;

        public function __construct(){
            $supplier = $this->fetchTestData();
            $this->name = $supplier['name'];
            $this->addrStreet = $supplier['addrStreet'];
            $this->addrCity = $supplier['addrCity'];
            $this->addrZip = $supplier['addrZip'];
            $this->regCode = $supplier['regCode'];
            $this->vatCode = $supplier['vatCode'];
        }
        
        private function fetchTestData(){
            return Array(
                'name' => 'Nejlepší, s.r.o.',
                'addrStreet' => 'Superulice 111',
                'addrCity' => 'Kocourkov',
                'addrZip' => '999 99',
                'regCode' => '987654321',
                'vatCode' => 'CZ987654321'
            );
        }
    }

