<?php

    class Customer {
        public $name,$addrStreet,$addrCity,$addrZip,$regCode,$vatCode;
       
        public function __construct(){
            $customer = $this->fetchTestData();
            $this->name = $customer['name'];
            $this->addrStreet = $customer['addrStreet'];
            $this->addrCity = $customer['addrCity'];
            $this->addrZip = $customer['addrZip'];
            $this->regCode = $customer['regCode'];
        }
        
        private function fetchTestData(){
            return Array(
                'name' => 'Nejhorší, s.r.o.',
                'addrStreet' => 'Tichá 999',
                'addrCity' => 'Twin Peaks',
                'addrZip' => '987 65',
                'regCode' => '123456789'
            );
        }
    }
