<?php

    class Data{
        public $number,$logo,$sign,$bankAccount,$vs,$payment,$dateCreated,$dateDue,$dateDuzp,$texBefore,$textAfter,$texPaid,$supplier,$customer,$item,$total,$total_vat,$paymantType,$currency;
        
        public function __construct(Supplier $supplier, Customer $customer,Item $item){
           // print_r($item);
            
            $this->testData($supplier,$customer,$item);
        }
        
        //přidal jsem vlastnost currency
        private function testData($suplier,$customer,$item){
            $this->number = "2021-0001";
            $this->logo = "static/img/logo.jpg";
            $this->sign = "static/img/sign.jpg"; 
            $this->bankAccount = "123-987415314/9876";
            $this->vs = 20210001;
            $this->dateCreated = "1. 3. 2021";
            $this->dateDue = "15. 3. 2021";
            $this->dateDuzp = "1. 3. 2021";
            $this->paymentType = "Převodem";
            $this->currency = "Kč";
            $this->supplier = $suplier;
            $this->customer = $customer;
            $this->item = $item; 
        }
    }

