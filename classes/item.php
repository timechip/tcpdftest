<?php
    class Item{
        public $items;
        
        public function __construct(){
            $this->items =  $this->fetchTestData();
        }
        
        public function fetchTestData(){
            return Array(
                Array(
                    'pocet' => 10,
                    'unit' => 'hod',
                    'text' => 'Kopání hrobů',
                    'vatRate' => 21,
                    'price' => 500.5,
                    'currency' => 'Kč',
                    'total' => 5005
                ),
                Array(
                    'pocet' => 50,
                    'unit' => 'hod',
                    'text' => 'Bouchání hřebíků',
                    'vatRate' => 15,
                    'price' => 5,
                    'currency' => 'Kč',
                    'total' => 250
                ),
                Array(
                    'pocet' => 13,
                    'unit' => 'hod',
                    'text' => 'Házení hrachu o stěnu',
                    'vatRate' => 10,
                    'price' => 65,
                    'currency' => 'Kč',
                    'total' => 845
                ),
                Array(
                    'pocet' => 5,
                    'unit' => 'hod',
                    'text' => 'Hrabání listí',
                    'vatRate' => 21,
                    'price' => 300,
                    'currency' => 'Kč',
                    'total' => 1500
                ),
                Array(
                    'pocet' => 19,
                    'unit' => 'hod',
                    'text' => 'Hrabání sněhu',
                    'vatRate' => 15,
                    'price' => 5.3,
                    'currency' => 'Kč',
                    'total' => 100,7
                ),
                Array(
                    'pocet' => 1000,
                    'unit' => 'hod',
                    'text' => 'Hádání se s manželkou',
                    'vatRate' => 10,
                    'price' => 100,
                    'currency' => 'Kč',
                    'total' => 100000
                    )
                );
            }
        }

