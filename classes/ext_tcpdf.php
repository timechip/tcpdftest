 <?php

 class EXT_TCPDF extends TCPDF
 {
    private $tcpdf;
    private $total21 = 0, $total15 = 0, $total10 = 0,$total = 0;
    
    public function __construct($pdfPageOrientation,$pdfUnit,$pdfPageFormat,$unicode,$encoding,$discache)
    {
        ob_end_clean();
        parent::__construct($pdfPageOrientation, $pdfUnit, $pdfPageFormat, $unicode, $encoding, $discache);
    }

    /**
     * Vygemerování celého PDF documentu
     * @param object $data Data faktury
     */ 
    public function generate(Data $data)
    {
        parent::setPrintHeader(false);
        parent::setPrintFooter(false);
        parent::SetFont('dejavusans', '', '');
        parent::AddPage();
        $html = $this->invoiceHead($data);
        $html .= $this->dataSuppilerCustomer($data);
        $html .= $this->Items($data);
        $html .= $this->finalPriceCounting($data);
        parent::writeHTML($html, true, false, true, false, '');
        parent::Output();
    }


    /**
     * Vygenerování hlavičky faktury
     * @param object $data Data faktury
     * @return string Horní část fuktury 
     */
    private function invoiceHead($data)
    {
        $html = '
        <style>'.file_get_contents('http://localhost/TCPDFTest/static/css/tcpdf.css').'</style>
        <table id="hlavicka-faktury" cellpadding="1px">
        <tr>
            <td rowspan="2"><img src="'.$data->logo.'" /></td>
            <td id="cislo-faktury"><div><span class="color-black">Faktura</span> '.$data->number.'</div></td></tr>
            <tr><td id="danovy-doklad" class="color-grey">DAŇOVÝ DOKLAD</td></tr>
        </table>';
        return $html;
    }


    /**
     * Rekapitulace faktury + výpočet jednotlivých sazeb DPH
     * @param object $data Data faktury
     * @return string Tabulka s rekapitulací jednotlivých pásem DPH 
     */
    private function Recap($data)
    {
        $html = "";
        if($this->total21 > 0){
            $vat21 = round($this->total21 * 0.21,1);
            $html.= '<tr><td>21%</td><td class="text-right">'.$this->numberFormat($this->total21).' '.$data->currency.'</td><td class="text-right">'.$this->numberFormat($vat21).' '.$data->currency.'</td></tr>';
            $this->total += $this->total21 + $vat21;
        }

        if($this->total15 > 0){
            $vat15 = round($this->total15 * 0.15,1);
            $html.= '<tr><td>15%</td><td class="text-right">'.$this->numberFormat($this->total15).' '.$data->currency.'</td><td class="text-right">'.$this->numberFormat($vat15).' '.$data->currency.'</td></tr>';
            $this->total += $this->total15 + $vat15;
        }

        if($this->total10 > 0){
            $vat10 = round($this->total10 * 0.1,1);
            $html.= '<tr><td>10%</td><td class="text-right">'.$this->numberFormat($this->total10).' '.$data->currency.'</td><td class="text-right">'.$this->numberFormat($vat10).' '.$data->currency.'</td></tr>';
            $this->total += $this->total10 + $vat10;
        }
        return $html;
    }


    /**
     * Vypočítání celkové výše faktury
     * @param object $data Data faktury
     * @return string Tabulka s výslednou cenou, QR kódem a podpisem
     */
    private function finalPriceCounting($data)
    {
        $html = '<table><tr><td>';
        $html .= '<table><tr><td>&nbsp;<br>'.$this->qrCode().'</td></tr></table></td>';
        $html .= '
        <td><table class="recap">
            <tr> 
                <td>&nbsp;<br>&nbsp;<br>SAZBA</td>
                <td class="text-right">&nbsp;<br>&nbsp;<br>ZÁKLAD</td>
                <td class="text-right">&nbsp;<br>&nbsp;<br>DPH</td>
            </tr>';
        $html .= $this->Recap($data);

        $html .= '
        <tr class="row-last-recap">
            <td colspan="3" class="text-right">'.$this->numberFormat($this->total).' Kč</td>
        </tr>
        <tr>
            <td colspan="3" class="text-right">&nbsp;<br>'.$this->Signature().'</td>
        </tr>
        </table></td></tr></table>';
        return $html;
    }


    /**
     * Simulovaný QR kód z obrázku
     * @return string Img QR kódu
     */
    private function qrCode()
    {
        $html = '<img src="static/img/qr.jpg" />';
        return $html;
    }

    
    /**
     * Simulovaný podpis z obrázku
     * @return string Img podpisu
     */
    private function Signature()
    {
        return '<img src="static/img/podpis.jpg" />';
    }


    /**
     * Fakturační údaje objednatele a dodavatele
     * @param object $data Data faktury
     * @return string Tabulka s fakturačními údaji
     */
    private function dataSuppilerCustomer($data)
    {
        $html = '
        <table id="supplier-customer">
            <tr id="row-first">
                <td colspan="2">&nbsp;<br>&nbsp;<br><div>DODAVATEL</div></td>
                <td colspan="2">&nbsp;<br>&nbsp;<br><div>ODBĚRATEL</div></td>
            </tr>
            <tr id="row-second">
                <td colspan="2">'.$data->supplier->name.'</td>        
                <td colspan="2">'.$data->customer->name.'</td>        
            </tr>
            <tr>
                <td colspan="2">'.$data->supplier->addrStreet.'</td>
                <td colspan="2">'.$data->customer->addrStreet.'</td>
            </tr>
            <tr id="row-fourth">
                <td colspan="2">'.$data->supplier->addrZip.' '.$data->supplier->addrCity.'</td>
                <td colspan="2">'.$data->customer->addrZip.' '.$data->customer->addrCity.'</td>
            </tr>
            <tr>
                <td width="25%">IČO</td><td width="25%" align="right">'.$data->supplier->regCode.'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td width="25%">IČO</td><td width="25%" align="right">'.$data->supplier->regCode.'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            </tr>
            <tr id="row-sixth">'
                .(isset($data->supplier->vatCode) ? ('<td width="25%">DIČ</td><td width="25%" align="right">'.$data->supplier->vatCode.'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>') : ("<td></td><td></td>"))
                .(isset($data->customer->vatCode) ? ('<td width="25%">DIČ</td><td width="25%" align="right">'.$data->customer->vatCode.'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>') : ("<td></td><td></td>")).'
            </tr>
            <tr>'
                .(isset($data->bankAccount) ? ('<td width="25%">Bankovní účet</td><td width="25%" align="right">'.$data->bankAccount.'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>') : ("<td></td><td></td>"))
                .'<td width="25%">Datum vystavení</td><td width="25%" align="right">'.$data->dateCreated.'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            </tr>
            <tr>
                <td width="25%">Variabilní symbol</td><td width="25%" align="right">'.$data->vs.'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td width="25%">Datum splatnosti</td><td width="25%" align="right">'.$data->dateDue.'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            </tr>
            <tr>
                <td width="25%">Způsob platby</td><td width="25%" align="right">'.$data->paymentType.'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td width="25%">DUZP</td><td width="25%" align="right">'.$data->dateDuzp.'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            </tr>
        </table>';
        return $html;
    }



    /**
     * Výpis jednotlivých položek faktury
     * @param object $data Data faktury
     * @return string Tabulka s jednotlivými položkami faktury
     */
    private function Items($data)
    {
        $html = '
        <table id="items">
        <tr id="row-first-items">
            <td colspan="6" class="text-left"><i>&nbsp;<br>&nbsp;<br>Fakturujeme Vám následující položky</i></td>
        </tr>
        <tr id="row-second-items">
            <td width="10%" class="text-right">POČ</td>        
            <td width="10%">MJ</td>        
            <td width="40%">TEXT</td>        
            <td width="5%" class="text-center">DPH</td>
            <td width="15%" class="text-right">CENA ZA MJ</td>
            <td width="20%" class="text-right">CELKEM BEZ DPH</td>
        </tr>';  
        $countItem = count($data->item->items);
        foreach($data->item as $arrData1){
            $i = 1;
            foreach($arrData1 as $arrData2){
                $html .= '<tr';
                if($i == 1){
                    $html .= ' class="row-first-items" ';
                }
                elseif($i == $countItem){
                    $html .= ' class="row-last-items" ';
                }
                $html .= '>';
                $html .= '<td class="text-right">'.$arrData2['pocet'].'</td>';
                $html .= '<td>'.$arrData2['unit'].'</td>';
                $html .= '<td>'.$arrData2['text'].'</td>';
                $html .= '<td class="text-center">'.$arrData2['vatRate'].'%</td>';
                $html .= '<td class="text-right">'.$this->numberFormat($arrData2['price']).' '.$data->currency.'</td>';
                $html .= '<td class="text-right">'.$this->numberFormat($arrData2['total']).' '.$data->currency.'</td>';
                $html .= '</tr>';
                switch ($arrData2['vatRate']){
                    case 21:
                        $this->total21 += $arrData2['total'];
                    break;
                    case 15:
                        $this->total15 += $arrData2['total'];
                    break;
                    case 10:
                        $this->total10 += $arrData2['total'];
                    break;
                } 
                $i++;
            }
        }
        $html .= '</table>';
        return $html;
    }

    /**
     * Modifikace nativní funkce numberFormat
     * @param int $number Finanční položka
     * @return string Zformátovaná finanční položka
     */
    private function numberFormat($number)
    {
        return number_format($number,2,","," ");
    }
}